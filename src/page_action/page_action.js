(() => {
  /**
   * @param {Function} fn
   */
  const ready = (fn) => {
    if (document.readyState != "loading") {
      fn();
    } else {
      document.addEventListener("DOMContentLoaded", fn);
    }
  };

  const panConfluenceSettingsKey = "pan.confluence.settings";
  const settingStorage = {
    /**
     * @param {string} key
     * @returns {Promise<string>}
     */
    getItem: (key) =>
      new Promise((resolve) => {
        return chrome.storage.local.get(
          [panConfluenceSettingsKey],
          (mainSettings) =>
            resolve(mainSettings[panConfluenceSettingsKey] ?? {})
        );
      }),
    /**
     * @param {string} key
     * @param {object} object
     * @returns {Promise}
     */
    setItem: (key, object) =>
      new Promise((resolve) => {
        chrome.storage.local.set({ [panConfluenceSettingsKey]: object }, () =>
          resolve()
        );
      }),
  };

  /**
   * @param {string} actionType
   * @return {Promise}
   */
  const firePerformActionEvent = (actionType) =>
    new Promise((resolve, reject) => {
      chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
        chrome.tabs.sendMessage(
          tabs[0].id,
          { type: "performAction", actionType },
          (response) => resolve()
        );
      });
    });

  /**
   * @param {HTMLInputElement} radioEl
   * @param {(actionType: string) => void} action
   */
  const addChangeEventListener = (radioEl, action) => {
    /**
     * @param {Event} event
     */
    const onChange = (event) => {
      if (event.target.checked) {
        action(radioEl.value);

        settingStorage.setItem(panConfluenceSettingsKey, {
          setting: radioEl.value,
        });
      }
    };
    radioEl.addEventListener("change", onChange);
  };

  ready(() => {
    const defaultOption = document.getElementById("pancRadioOptionDefault");
    const stickyOption = document.getElementById("pancRadioOptionSticky");
    const fixedOption = document.getElementById("pancRadioOptionFixed");

    // init
    settingStorage
      .getItem(panConfluenceSettingsKey)
      .then((panConfluenceSettings) => {
        defaultOption.checked =
          panConfluenceSettings.setting !== "sticky" ||
          panConfluenceSettings.setting !== "fixed";
        stickyOption.checked = panConfluenceSettings.setting === "sticky";
        fixedOption.checked = panConfluenceSettings.setting === "fixed";

        addChangeEventListener(defaultOption, firePerformActionEvent);
        addChangeEventListener(stickyOption, firePerformActionEvent);
        addChangeEventListener(fixedOption, firePerformActionEvent);
      });
  });
})();
