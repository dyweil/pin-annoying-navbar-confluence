(() => {
  const ready = (fn) => {
    if (document.readyState != "loading") {
      fn();
    } else {
      document.addEventListener("DOMContentLoaded", fn);
    }
  };

  //
  chrome.runtime.sendMessage({ type: "init" }, (_response) =>
    ready(() => {
      const contentHeaderContainer = document.getElementById(
        "content-header-container"
      );
      if (!contentHeaderContainer) {
        return;
      }

      // perform actual action on init
      settingStorage
        .getItem(panConfluenceSettingsKey)
        .then(({ setting }) => performAction(setting, contentHeaderContainer));

      // perform actual action on event
      chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
        if (request.type === "performAction") {
          performAction(request.actionType, contentHeaderContainer);
          sendResponse();
        }
      });
    })
  );

  /**
   * @param {string} actionType
   * @param {HTMLElement} navbar
   */
  const performAction = (actionType, navbar) => {
    const supportedActionTypes = ["default", "sticky", "fixed"];
    if (!supportedActionTypes.includes(actionType)) {
      console.warn(`Unsupported PAN Confluence action type: ${actionType}`);
      return;
    }

    switch (actionType) {
      case "default":
        navbar.id = "content-header-container";
        navbar.className = navbar.className.replaceAll("sticky-header", "");
        navbar.parentElement.style.animationName = "";
        navbar.parentElement.style.position = "";
        break;

      case "sticky":
        navbar.id = "content-header-container";
        navbar.className += " sticky-header";
        navbar.parentElement.style.animationName = "stickyAppear";
        break;

      case "fixed":
        navbar.id = "content-header-container-fixed";
        navbar.className = navbar.className.replaceAll("sticky-header", "");
        navbar.parentElement.style.animationName = "";
        navbar.parentElement.style.position = "";
        break;

      default:
        break;
    }
  };

  const panConfluenceSettingsKey = "pan.confluence.settings";
  const settingStorage = {
    /**
     * @param {string} key
     * @returns {Promise<string>}
     */
    getItem: (key) =>
      new Promise((resolve) => {
        return chrome.storage.local.get(
          [panConfluenceSettingsKey],
          (mainSettings) =>
            resolve(mainSettings[panConfluenceSettingsKey] ?? {})
        );
      }),
    /**
     * @param {string} key
     * @param {object} object
     * @returns {Promise}
     */
    setItem: (key, object) =>
      new Promise((resolve) => {
        chrome.storage.local.set({ [panConfluenceSettingsKey]: object }, () =>
          resolve()
        );
      }),
  };
})();
